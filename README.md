# Bank Account Application

Application will be available at `http://localhost:8080/`

### Customers

* Create customer: 

| HTTP Method | Endpoint   | Request body |
| ----------- | ---------- | ------------ |
| POST        | /customers | { "firstName" : string, "lastName" : string, "email": string } |

* Update customer: 

| HTTP Method | Endpoint                | Request body |
| ----------- | ----------------------- | ------------ |
| PUT         | /customers/{customerId} | { "firstName" : string, "lastName" : string, "email": string } |

* Get customer: 

| HTTP Method | Endpoint                |
| ----------- | ----------------------- |
| GET         | /customers/{customerId} |

* Get customers with account details: 

| HTTP Method | Endpoint            |
| ----------- | ------------------- |
| GET         | /customers/accounts |

* Delete customer: 

| HTTP Method | Endpoint                |
| ----------- | ----------------------- |
| DELETE      | /customers/{customerId} |

### Account

* Create account for customer: 

| HTTP Method | Endpoint                         | Request body |
| ----------- | -------------------------------- | ------------ |
| POST        | /customers/{customerId}/accounts | { "accountType" : string, "balance" : double } |

* Transfer funds from one account to another: 

| HTTP Method | Endpoint                         | Request body |
| ----------- | -------------------------------- | ------------ |
| PUT         | /accounts/transferFund           | { "fromAccount" : long, "toAccount" : long, "amount": double } |

* Get all accounts

| HTTP Method | Endpoint      | 
| ----------- | ------------- | 
| GET         | /accounts     | 

* Get account by accountNumber

| HTTP Method | Endpoint                  | 
| ----------- | ------------------------- | 
| GET         | /accounts/{accountNumber} | 

