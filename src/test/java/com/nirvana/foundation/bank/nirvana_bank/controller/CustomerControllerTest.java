package com.nirvana.foundation.bank.nirvana_bank.controller;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nirvana.foundation.bank.nirvana_bank.dto.CustomerDTO;
import com.nirvana.foundation.bank.nirvana_bank.entity.Account;
import com.nirvana.foundation.bank.nirvana_bank.entity.AccountType;
import com.nirvana.foundation.bank.nirvana_bank.entity.Customer;
import com.nirvana.foundation.bank.nirvana_bank.repository.AccountRepository;
import com.nirvana.foundation.bank.nirvana_bank.repository.CustomerRepository;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerTest {
	
	private static final String CUSTOMER_URL = "/customers";
	private static final String CUSTOMER_ID_URL = CUSTOMER_URL + "/%d";
	private static final String CUSTOMER_ACCOUNTS = CUSTOMER_URL + "/accounts";
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Order(1)
	@Test
	public void testCreateCustomer() throws Exception {
		
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setEmail("myEmail");
		customerDTO.setFirstName("Name");
		customerDTO.setLastName("LastName");
		
		String requestBody = objectMapper.writeValueAsString(customerDTO);
		
		mockMvc.perform(
			post(CUSTOMER_URL)
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isCreated())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.email", is("myEmail")))
		.andExpect(jsonPath("$.firstName", is("Name")))
		.andExpect(jsonPath("$.lastName", is("LastName")));
	}
	
	@Order(2)
	@Test
	public void testUpdateCustomer() throws Exception {
		
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setEmail("email@email");
		customerDTO.setFirstName("Name");
		customerDTO.setLastName("LastName");
		
		String requestBody = objectMapper.writeValueAsString(customerDTO);
		
		mockMvc.perform(
			put(String.format(CUSTOMER_ID_URL, 1))
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.email", is("email@email")))
		.andExpect(jsonPath("$.firstName", is("Name")))
		.andExpect(jsonPath("$.lastName", is("LastName")));
	}
	
	@Transactional
	@Order(3)
	@Test
	public void testGetCustomers() throws Exception {
		
		Customer customer = customerRepository.findById(1).get();
		
		Account toAccount = new Account();
		toAccount.setAccountType(AccountType.SAVINGS);
		toAccount.setBalance(50.0);
		toAccount.addCustomer(customer);
		
		customer.addAccount(toAccount);
		toAccount = accountRepository.save(toAccount);
		
		mockMvc.perform(
			get(CUSTOMER_ACCOUNTS)
		).andExpect(status().isOk())
		.andExpect(jsonPath("$[0].id", is(1)))
		.andExpect(jsonPath("$[0].email", is("email@email")))
		.andExpect(jsonPath("$[0].firstName", is("Name")))
		.andExpect(jsonPath("$[0].lastName", is("LastName")))
		.andExpect(jsonPath("$[0].accounts[0].balance", is(50.0)));
	}
	
	@Order(4)
	@Test
	public void testGetCustomerById() throws Exception {
		mockMvc.perform(
			get(String.format(CUSTOMER_ID_URL, 1))
		).andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.email", is("email@email")))
		.andExpect(jsonPath("$.firstName", is("Name")))
		.andExpect(jsonPath("$.lastName", is("LastName")));
	}
	
	@Order(5)
	@Test
	public void testDeleteCustomer() throws Exception {
		mockMvc.perform(
			delete(String.format(CUSTOMER_ID_URL, 1))
		).andExpect(status().isOk());
		
		assertFalse(customerRepository.findById(1).isPresent());
	}

}
