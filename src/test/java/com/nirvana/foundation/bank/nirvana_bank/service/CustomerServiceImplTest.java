package com.nirvana.foundation.bank.nirvana_bank.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nirvana.foundation.bank.nirvana_bank.dto.CustomerDTO;
import com.nirvana.foundation.bank.nirvana_bank.entity.Customer;
import com.nirvana.foundation.bank.nirvana_bank.exception.ResourceNotFoundException;
import com.nirvana.foundation.bank.nirvana_bank.repository.CustomerRepository;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTest {

	@InjectMocks
	private CustomerServiceImpl customerService;
	
	@Mock
	private CustomerRepository mockCustomerRepository;
	
	@Test
	public void testCreateCustomer() {
		
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setEmail("myEmail");
		customerDTO.setFirstName("Name");
		customerDTO.setLastName("LastName");
		
		when(mockCustomerRepository.save(any(Customer.class))).thenReturn(new Customer());
		
		customerService.createCustomer(customerDTO);
		
		verify(mockCustomerRepository, times(1)).save(any(Customer.class));
	}
	
	@Test
	public void testUpdateCustomer() {
		
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setEmail("myEmail");
		customerDTO.setFirstName("Name");
		customerDTO.setLastName("LastName");
		
		when(mockCustomerRepository.findById(anyInt())).thenReturn(Optional.of(new Customer()));
		when(mockCustomerRepository.save(any(Customer.class))).thenReturn(new Customer());
		
		customerService.updateCustomer(1, customerDTO);
		
		verify(mockCustomerRepository, times(1)).save(any(Customer.class));
	}
	
	@Test
	public void testUpdateCustomerForCustomerNotFound() {
		
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setEmail("myEmail");
		customerDTO.setFirstName("Name");
		customerDTO.setLastName("LastName");
		
		when(mockCustomerRepository.findById(anyInt())).thenReturn(Optional.empty());
		
		assertThrows(ResourceNotFoundException.class, () -> {
			customerService.updateCustomer(1, customerDTO);
		});
	}
	
	@Test
	public void testFindCustomerById() {
		when(mockCustomerRepository.findById(anyInt())).thenReturn(Optional.of(new Customer()));
		
		customerService.findCustomerById(1);
		
		verify(mockCustomerRepository, times(1)).findById(anyInt());
	}
	
	@Test
	public void testFindCustomerByIdWithIdNotFound() {
		when(mockCustomerRepository.findById(anyInt())).thenReturn(Optional.empty());
		
		assertThrows(ResourceNotFoundException.class, () -> {
			customerService.findCustomerById(1);
		});
		
		verify(mockCustomerRepository, times(1)).findById(anyInt());
	}
	
	@Test
	public void testFindAllCustomers() {
		customerService.findCustomers();
		verify(mockCustomerRepository, times(1)).findAll();
	}
	
	@Test
	public void deleteCustomer() {
		when(mockCustomerRepository.findById(anyInt())).thenReturn(Optional.of(new Customer()));
		
		customerService.deleteCustomer(1);
		
		verify(mockCustomerRepository, times(1)).delete(any(Customer.class));
	}
	
	@Test
	public void deleteCustomerWithIdNotFound() {
		when(mockCustomerRepository.findById(anyInt())).thenReturn(Optional.empty());
		
		assertThrows(ResourceNotFoundException.class, () -> {
			customerService.deleteCustomer(1);
		});
		
		verify(mockCustomerRepository, never()).delete(any(Customer.class));
	}

}
