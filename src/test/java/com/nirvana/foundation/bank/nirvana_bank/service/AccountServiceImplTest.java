package com.nirvana.foundation.bank.nirvana_bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nirvana.foundation.bank.nirvana_bank.dto.AccountDTO;
import com.nirvana.foundation.bank.nirvana_bank.dto.Transfer;
import com.nirvana.foundation.bank.nirvana_bank.entity.Account;
import com.nirvana.foundation.bank.nirvana_bank.entity.AccountType;
import com.nirvana.foundation.bank.nirvana_bank.entity.Customer;
import com.nirvana.foundation.bank.nirvana_bank.exception.BusinessException;
import com.nirvana.foundation.bank.nirvana_bank.exception.ResourceNotFoundException;
import com.nirvana.foundation.bank.nirvana_bank.repository.AccountRepository;

@ExtendWith(MockitoExtension.class)
public class AccountServiceImplTest {

	@InjectMocks
	private AccountServiceImpl accountService;
	
	@Mock
	private CustomerService mockCustomerService;
	
	@Mock
	private AccountRepository mockAccountRepository;
	
	@Test
	public void testAddAccount() {
		
		AccountDTO account = new AccountDTO();
		account.setAccountType(AccountType.CURRENT);
		account.setBalance(0.0);
		
		when(mockCustomerService.findCustomerById(anyInt())).thenReturn(new Customer());
		when(mockAccountRepository.save(any(Account.class))).thenReturn(new Account());
		
		accountService.addAccount(1, account);
		
		verify(mockAccountRepository, times(1)).save(any(Account.class));
	}
	
	@Test
	public void testAddAccountForCustomerNotFound() {
		
		AccountDTO account = new AccountDTO();
		account.setAccountType(AccountType.CURRENT);
		account.setBalance(0.0);
		
		when(mockCustomerService.findCustomerById(anyInt())).thenThrow(ResourceNotFoundException.class);
		
		assertThrows(ResourceNotFoundException.class, () -> {
			accountService.addAccount(1, account);
		});
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testTransfer() {
		
		Account fromAccount = new Account();
		fromAccount.setAccountType(AccountType.CURRENT);
		fromAccount.setBalance(10.0);
		
		Account toAccount = new Account();
		toAccount.setAccountType(AccountType.SAVINGS);
		toAccount.setBalance(20.0);
		
		Transfer transfer = new Transfer();
		transfer.setAmount(5.0);
		transfer.setFromAccount(1L);
		transfer.setToAccount(2L);
		
		when(mockAccountRepository.findById(anyLong())).thenReturn(Optional.of(fromAccount), Optional.of(toAccount));
		when(mockAccountRepository.save(any(Account.class))).thenReturn(new Account());
		
		ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
		
		accountService.transfer(transfer);
		
		verify(mockAccountRepository, times(2)).save(accountCaptor.capture());
		
		List<Account> accounts = accountCaptor.getAllValues();
		assertEquals(5.0, accounts.get(0).getBalance());
		assertEquals(25.0, accounts.get(1).getBalance());
	}
	
	@Test
	public void testTransferForSameAccount() {
		
		Transfer transfer = new Transfer();
		transfer.setAmount(5.0);
		transfer.setFromAccount(1L);
		transfer.setToAccount(1L);
		
		assertThrows(BusinessException.class, () -> {
			accountService.transfer(transfer);
		});
		
		verify(mockAccountRepository, never()).save(any(Account.class));
	}
	
	@Test
	public void testTransferForAmountNotEnough() {
		
		Account fromAccount = new Account();
		fromAccount.setAccountType(AccountType.CURRENT);
		fromAccount.setBalance(10.0);
		
		Transfer transfer = new Transfer();
		transfer.setAmount(15.0);
		transfer.setFromAccount(1L);
		transfer.setToAccount(2L);
		
		when(mockAccountRepository.findById(anyLong())).thenReturn(Optional.of(fromAccount));
		
		assertThrows(BusinessException.class, () -> {
			accountService.transfer(transfer);
		});
		
		verify(mockAccountRepository, never()).save(any(Account.class));
	}
	
	@Test
	public void testTransferForAccountNotFound() {
		
		Account fromAccount = new Account();
		fromAccount.setAccountType(AccountType.CURRENT);
		fromAccount.setBalance(10.0);
		
		Account toAccount = new Account();
		toAccount.setAccountType(AccountType.SAVINGS);
		toAccount.setBalance(20.0);
		
		Transfer transfer = new Transfer();
		transfer.setAmount(15.0);
		transfer.setFromAccount(1L);
		transfer.setToAccount(2L);
		
		when(mockAccountRepository.findById(anyLong())).thenReturn(Optional.empty());
		
		assertThrows(ResourceNotFoundException.class, () -> {
			accountService.transfer(transfer);
		});
		
		verify(mockAccountRepository, never()).save(any(Account.class));
	}
	
	@Test
	public void testFindAllAccounts() {
		accountService.getAllAccounts();
		verify(mockAccountRepository, times(1)).findAll();
	}

}
