package com.nirvana.foundation.bank.nirvana_bank.controller;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nirvana.foundation.bank.nirvana_bank.dto.AccountDTO;
import com.nirvana.foundation.bank.nirvana_bank.dto.Transfer;
import com.nirvana.foundation.bank.nirvana_bank.entity.Account;
import com.nirvana.foundation.bank.nirvana_bank.entity.AccountType;
import com.nirvana.foundation.bank.nirvana_bank.entity.Customer;
import com.nirvana.foundation.bank.nirvana_bank.repository.AccountRepository;
import com.nirvana.foundation.bank.nirvana_bank.repository.CustomerRepository;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest {
	
	private static final String ADD_ACCOUNT_URL = "/customers/%d/accounts";
	private static final String TRANSFER_URL = "/accounts/transferFund";
	private static final String ACCOUNTS_URL = "/accounts";
	private static final String ACCOUNTS_ID_URL = ACCOUNTS_URL + "/%d";
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Order(1)
	@Test
	public void testAddAcountForCustomer() throws Exception {
		
		Customer customer = new Customer();
		customer.setEmail("myEmail");
		customer.setFirstName("Name");
		customer.setLastName("LastName");
		customerRepository.save(customer);
		
		AccountDTO account = new AccountDTO();
		account.setAccountType(AccountType.CURRENT);
		account.setBalance(10.0);
		
		String requestBody = objectMapper.writeValueAsString(account);
		
		mockMvc.perform(
			post(String.format(ADD_ACCOUNT_URL, 1))
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isCreated())
		.andExpect(jsonPath("$.accountNumber", is(1)))
		.andExpect(jsonPath("$.accountType", is(AccountType.CURRENT.toString())))
		.andExpect(jsonPath("$.balance", is(10.0)));
	}
	
	@Transactional
	@Test
	public void testTransferFund() throws Exception {
		
		Customer customer = new Customer();
		customer.setEmail("myEmail");
		customer.setFirstName("OtherUser");
		customer.setLastName("LastName");
		customerRepository.save(customer);
		
		Account toAccount = new Account();
		toAccount.setAccountType(AccountType.SAVINGS);
		toAccount.setBalance(50.0);
		toAccount.addCustomer(customer);
		
		customer.addAccount(toAccount);
		toAccount = accountRepository.save(toAccount);
		
		Transfer transfer = new Transfer();
		transfer.setAmount(5.0);
		transfer.setFromAccount(1L);
		transfer.setToAccount(toAccount.getAccountNumber());
		
		String requestBody = objectMapper.writeValueAsString(transfer);
		
		mockMvc.perform(
			put(TRANSFER_URL)
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isOk());
		
		Optional<Account> resultFrom = accountRepository.findById(1L);
		Optional<Account> resultTo = accountRepository.findById(toAccount.getAccountNumber());
		
		assertTrue(resultFrom.isPresent());
		assertTrue(resultTo.isPresent());
		
		assertAll(
			() -> assertEquals(5.0, resultFrom.get().getBalance()),
			() -> assertEquals(55.0, resultTo.get().getBalance())
		);
	}
	
	@Test
	public void testGetAccounts() throws Exception {
		mockMvc.perform(
			get(ACCOUNTS_URL)
		).andExpect(status().isOk())
		.andExpect(jsonPath("$[0].accountNumber", is(1)))
		.andExpect(jsonPath("$[0].accountType", is(AccountType.CURRENT.toString())))
		.andExpect(jsonPath("$[0].balance", is(10.0)));
	}
	
	@Test
	public void testGetAccountById() throws Exception {
		mockMvc.perform(
			get(String.format(ACCOUNTS_ID_URL, 1))
		).andExpect(status().isOk())
		.andExpect(jsonPath("$.accountNumber", is(1)))
		.andExpect(jsonPath("$.accountType", is(AccountType.CURRENT.toString())))
		.andExpect(jsonPath("$.balance", is(10.0)));
	}

}
