package com.nirvana.foundation.bank.nirvana_bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nirvana.foundation.bank.nirvana_bank.dto.CustomerDTO;
import com.nirvana.foundation.bank.nirvana_bank.entity.Customer;
import com.nirvana.foundation.bank.nirvana_bank.service.CustomerService;


@RestController
@RequestMapping(path = "/customers")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@PostMapping
	public ResponseEntity<Customer> createCustomer(@RequestBody CustomerDTO customerDTO) {
		Customer customer = customerService.createCustomer(customerDTO);
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);		
	}	
	
	@PutMapping(path = "/{id}")
	public ResponseEntity<Customer> updateCustomer(
			@PathVariable Integer id, @RequestBody CustomerDTO customerDTO) {
		Customer customer = customerService.updateCustomer(id, customerDTO);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);		
	}	
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<Customer> findCustomer(@PathVariable Integer id) {
		Customer customer = customerService.findCustomerById(id);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);		
	}
	
	@GetMapping(path = "/accounts")
	public ResponseEntity<List<Customer>> findAllCustomers() {
		List<Customer> customers = customerService.findCustomers();
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);		
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Customer> deleteCustomer(@PathVariable Integer id) {
		customerService.deleteCustomer(id);
		return new ResponseEntity<Customer>(HttpStatus.OK);
	}
}
