package com.nirvana.foundation.bank.nirvana_bank.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class BankExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = ResourceNotFoundException.class)
	public ResponseEntity<Object> handleResourceNotFoundException(RuntimeException ex, WebRequest request) {
		String bodyOfResponse = ex.getMessage() != null ? ex.getMessage() : "Resource not found.";
		return handleExceptionInternal(ex, bodyOfResponse, 
				new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	@ExceptionHandler(value = BusinessException.class)
	public ResponseEntity<Object> handleBusinessException(RuntimeException ex, WebRequest request) {
		return handleExceptionInternal(ex, ex.getMessage(), 
				new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

}
