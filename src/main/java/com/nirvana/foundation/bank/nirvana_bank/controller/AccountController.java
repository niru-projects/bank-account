package com.nirvana.foundation.bank.nirvana_bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nirvana.foundation.bank.nirvana_bank.dto.AccountDTO;
import com.nirvana.foundation.bank.nirvana_bank.dto.Transfer;
import com.nirvana.foundation.bank.nirvana_bank.entity.Account;
import com.nirvana.foundation.bank.nirvana_bank.service.AccountService;

@RestController
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	@PostMapping(path = "/customers/{customerId}/accounts")
	public ResponseEntity<Account> addAccountForCustomer(@PathVariable Integer customerId, @RequestBody AccountDTO accountDTO) {
		Account account = accountService.addAccount(customerId, accountDTO);
		return new ResponseEntity<Account>(account, HttpStatus.CREATED);		
	}
	
	@PutMapping(path = "/accounts/transferFund")
	public ResponseEntity<Object> transferFunds(@RequestBody Transfer transfer) {
		accountService.transfer(transfer);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@GetMapping(path = "/accounts")
	public ResponseEntity<List<Account>> getAllAccounts() {
		List<Account> accounts = accountService.getAllAccounts();
		return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);		
	} 
	
	@GetMapping(path = "/accounts/{accountNumber}")
	public ResponseEntity<Account> getAccountBalance(@PathVariable Long accountNumber) {
		Account account = accountService.findAccountByAccountNumber(accountNumber);
		return new ResponseEntity<Account>(account, HttpStatus.OK);		
	}  
}
