package com.nirvana.foundation.bank.nirvana_bank.dto;

import lombok.Data;

@Data
public class CustomerDTO {
	
	private String firstName;
	private String lastName;
	private String email;
}
