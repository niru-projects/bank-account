package com.nirvana.foundation.bank.nirvana_bank.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nirvana.foundation.bank.nirvana_bank.dto.AccountDTO;
import com.nirvana.foundation.bank.nirvana_bank.dto.Transfer;
import com.nirvana.foundation.bank.nirvana_bank.entity.Account;
import com.nirvana.foundation.bank.nirvana_bank.entity.Customer;
import com.nirvana.foundation.bank.nirvana_bank.exception.BusinessException;
import com.nirvana.foundation.bank.nirvana_bank.exception.ResourceNotFoundException;
import com.nirvana.foundation.bank.nirvana_bank.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private AccountRepository repository;

	@Override
	public Account addAccount(Integer customerId, AccountDTO accountDTO) {
		
		Customer customer = customerService.findCustomerById(customerId);
		
		Account account = new Account();
		account.setAccountType(accountDTO.getAccountType());
		account.setBalance(accountDTO.getBalance());
		account.addCustomer(customer);
		
		customer.addAccount(account);
		
		return repository.save(account);
	}

	@Override
	public void transfer(Transfer transfer) {
		
		if(transfer.getFromAccount().equals(transfer.getToAccount())) {
			throw new BusinessException("Accounts must be different");
		}
		
		Account fromAccount = findAccountByAccountNumber(transfer.getFromAccount());
		
		if(transfer.getAmount() > fromAccount.getBalance()) {
			throw new BusinessException("Account " + fromAccount + " doesn't have enough to transfer.");
		}
		
		Account toAccount = findAccountByAccountNumber(transfer.getToAccount());

		fromAccount.removeAmount(transfer.getAmount());
		toAccount.addAmount(transfer.getAmount());
		
		repository.save(fromAccount);
		repository.save(toAccount);
	}

	@Override
	public List<Account> getAllAccounts() {
		return repository.findAll();
	}

	@Override
	public Account findAccountByAccountNumber(Long accountNumber) {
		Optional<Account> opAccount = repository.findById(accountNumber);
		if(!opAccount.isPresent()) {
			throw new ResourceNotFoundException("Account doesn't exist.");
		}
		return opAccount.get();
	}
	
}
