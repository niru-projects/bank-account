package com.nirvana.foundation.bank.nirvana_bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nirvana.foundation.bank.nirvana_bank.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
	
}
