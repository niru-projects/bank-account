package com.nirvana.foundation.bank.nirvana_bank.dto;

import lombok.Data;

@Data
public class Transfer {
	
	private Long fromAccount;
	private Long toAccount;
	private double amount;
}
