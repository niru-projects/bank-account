package com.nirvana.foundation.bank.nirvana_bank.service;

import java.util.List;

import com.nirvana.foundation.bank.nirvana_bank.dto.CustomerDTO;
import com.nirvana.foundation.bank.nirvana_bank.entity.Customer;

public interface CustomerService {

	Customer createCustomer(CustomerDTO customerDTO);

	Customer updateCustomer(Integer id, CustomerDTO customerDTO);

	Customer findCustomerById(Integer id);

	List<Customer> findCustomers();

	void deleteCustomer(Integer id);

}
