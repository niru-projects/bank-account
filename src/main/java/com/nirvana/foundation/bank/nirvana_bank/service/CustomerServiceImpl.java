package com.nirvana.foundation.bank.nirvana_bank.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nirvana.foundation.bank.nirvana_bank.dto.CustomerDTO;
import com.nirvana.foundation.bank.nirvana_bank.entity.Customer;
import com.nirvana.foundation.bank.nirvana_bank.exception.ResourceNotFoundException;
import com.nirvana.foundation.bank.nirvana_bank.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository repository;
	
	@Override
	public Customer createCustomer(CustomerDTO customerDTO) {
		Customer customer = new Customer();
		return saveCustomer(customerDTO, customer);
	}
	
	@Override
	public Customer updateCustomer(Integer id, CustomerDTO customerDTO) {
		Customer customer = findCustomerById(id);
		return saveCustomer(customerDTO, customer);
	}

	@Override
	public Customer findCustomerById(Integer id) {
		Optional<Customer> opCustomer = repository.findById(id);
		if(!opCustomer.isPresent()) {
			throw new ResourceNotFoundException("Customer doesn't exist.");
		}
		
		return opCustomer.get();
	}

	@Override
	public List<Customer> findCustomers() {
		return repository.findAll();
	}

	@Override
	public void deleteCustomer(Integer id) {
		Customer customer = findCustomerById(id);
		repository.delete(customer);
	}
	
	private Customer saveCustomer(CustomerDTO customerDTO, Customer customer) {
		customer.setFirstName(customerDTO.getFirstName());
		customer.setLastName(customerDTO.getLastName());
		customer.setEmail(customerDTO.getEmail());
		return repository.save(customer);
	}
	
}
