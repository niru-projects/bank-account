package com.nirvana.foundation.bank.nirvana_bank.dto;

import com.nirvana.foundation.bank.nirvana_bank.entity.AccountType;

import lombok.Data;

@Data
public class AccountDTO {
	
	private AccountType accountType;
	private double balance;
}
