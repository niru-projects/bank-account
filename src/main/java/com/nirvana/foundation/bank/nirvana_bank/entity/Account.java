package com.nirvana.foundation.bank.nirvana_bank.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(of = "accountNumber")
public class Account implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long accountNumber;
	
	@Enumerated(EnumType.STRING)
	private AccountType accountType;
	
	@Column
	private double balance;
	
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "customer_accounts", 
		joinColumns = @JoinColumn(name = "account_number"), 
		inverseJoinColumns = @JoinColumn(name = "customer_id"))
	private Set<Customer> customers;
	
	public void addCustomer(Customer customer) {
		if(customers == null) {
			customers = new HashSet<>();
		}
		customers.add(customer);
	}
	
	public void addAmount(double amount) {
		balance += amount;
	}
	
	public void removeAmount(double amount) {
		balance -= amount;
	}
}
