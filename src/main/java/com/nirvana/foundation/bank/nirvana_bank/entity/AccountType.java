package com.nirvana.foundation.bank.nirvana_bank.entity;

public enum AccountType {
	
	SAVINGS,
	CURRENT

}
