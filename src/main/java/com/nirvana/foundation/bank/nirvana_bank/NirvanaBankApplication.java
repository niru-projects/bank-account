package com.nirvana.foundation.bank.nirvana_bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NirvanaBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(NirvanaBankApplication.class, args);
	}

}
