package com.nirvana.foundation.bank.nirvana_bank.service;

import java.util.List;

import com.nirvana.foundation.bank.nirvana_bank.dto.AccountDTO;
import com.nirvana.foundation.bank.nirvana_bank.dto.Transfer;
import com.nirvana.foundation.bank.nirvana_bank.entity.Account;

public interface AccountService {

	Account addAccount(Integer customerId, AccountDTO accountDTO);

	void transfer(Transfer transfer);

	List<Account> getAllAccounts();

	Account findAccountByAccountNumber(Long accountNumber);

}
